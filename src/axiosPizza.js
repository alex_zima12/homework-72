import axios from 'axios';

const axiosPizza = axios.create({
    baseURL: 'https://homeworks-6270c.firebaseio.com/'
});

export default axiosPizza;