import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes,removeDish} from "../../store/actions/dishesActions/dishesActions";
import Spinner from "../Spinner/Spinner";
import MenuItem from "../MenuItem/MenuItem";
import axiosPizza from "../../axiosPizza";

const DishesList = props => {
    const dishes = useSelector(state => state.dishes.dishes);
    const loading = useSelector(state => state.dishes.loading);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const removeDishHandler = async index => {
        dispatch(removeDish(index));
        await axiosPizza.delete('/turtlePizza/' + index + '.json');
    };

    let arr = []
    if (dishes) {
        Object.keys(dishes).forEach(function (dish,index) {
            arr.push(<MenuItem
                id={dish}
                title={this[dish].title}
                price={this[dish].price}
                image={this[dish].image}
                key={index}
                deleteItemFromOrder={() => removeDishHandler(dish)}
            />)

        }, dishes);
    }

     if (loading) {
         arr = <Spinner/>;
    }
    return (
        <>
            {arr}
        </>
    );
};

export default DishesList;