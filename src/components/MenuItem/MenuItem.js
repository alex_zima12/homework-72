import React from 'react';
import {NavLink} from 'react-router-dom';

const MenuItem = props => {

    return (
        <div className="card w-100 d-flex p-3" id={props.id}>
            <img src={props.image} className="card-img-top " alt="dishPhoto" style={{width: 200}}/>
            <div className="card-body d-flex flex-column">
                <h5 className="card-title"> Title: {props.title}</h5>
                <p className="card-text"> Price: {props.price} KGS</p>
            </div>
            <div className="d-flex flex-row-reverse ">
                <NavLink className="btn btn-primary m-2" to={"/" + props.id + "/edit"}>
                    Edit
                </NavLink>
                <button className="btn btn-danger m-2" type="submit"
                        onClick={() => props.deleteItemFromOrder(props.id)}
                >Delete
                </button>
            </div>
        </div>

    );
};

export default MenuItem;