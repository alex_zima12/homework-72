import React from 'react';
import DishesList from "../DishesList/DishesList";
import {setPurchasing} from "../../store/actions/dishesActions/dishesActions";
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import DishData from "../../container/DishData/DishData";

const DishBuilder = () => {
    const purchasing = useSelector(state => state.dishes.purchasing);
    const dispatch = useDispatch();

    const purchaseOpenHandler = () => {
        dispatch(setPurchasing(true));
    };

    const purchaseCancelHandler = () => {
        dispatch(setPurchasing(false));
    };

    return (

        <div className="container">
            <div className="d-flex flex-row-reverse bd-highlight mb-3">
                <button type="button" className="btn btn-primary btn-lg m-3" onClick={purchaseOpenHandler}>Add new dish</button>
            </div>
            <Modal show={purchasing} closed={purchaseCancelHandler}>
                <DishData
                    purchaseCancelled={purchaseCancelHandler}
                />
            </Modal>
            <DishesList/>
        </div>
    );
};

export default DishBuilder;