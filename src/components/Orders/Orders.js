import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {removeOrder,fetchData} from "../../store/actions/ordersActions/ordersActions";
import axiosPizza from "../../axiosPizza";
import {fetchDishes} from "../../store/actions/dishesActions/dishesActions";

const Orders = () => {

    const orders = useSelector(state => state.orders.orders);
    const dishes = useSelector(state => state.dishes.dishes);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchDishes())
        dispatch(fetchData());
    }, [dispatch]);

    let arrOrd = []
    if (orders) {
        Object.keys(orders).forEach(function (order) {
            let orderTransform = {key: order, total: 0, deliver: 150, dishes: []}
            Object.keys(orders[order]).forEach(function (dishesInn) {
                let dishe = dishes[dishesInn];
                if (dishe && dishe.title) {
                    let localPrice = parseInt(dishe.price) * parseInt(orders[order][dishesInn]);
                    orderTransform.dishes.push({name: dishe.title, price: localPrice})
                    orderTransform.total += localPrice
                }
            }, orders[order]);
            arrOrd.push(orderTransform)
        }, orders);
    }

    const deleteOrder = async  id => {
        dispatch(removeOrder(id));
        await axiosPizza.delete('/turtlePizzaOrders/' + id + '.json');
    };

    let arrShowOrders = []
    if (dishes) {
        arrOrd.map(ord => {
            let dishList = []
            ord.dishes.map((dish,index) => {
                return dishList.push(<p key={index}><b>{dish.name}</b> {dish.price} </p>)
            })
            return arrShowOrders.push(
                <div className="container border border-primary mt-3" key={ord.key}>
                    <p> Number: {ord.key}</p>
                    <p> Delivery: {ord.deliver}</p>
                    <div className="border border-secondary p-2">  Order: {dishList}</div>
                    <p> Total price: {ord.total + ord.deliver }</p>
                    <button className="btn btn-danger m-3" onClick={ () => deleteOrder(ord.key) }>DELETE ORDER</button>
                </div>)
        })
    }

    return (
        <div>
            {arrShowOrders}
        </div>
    );
};

export default Orders;
