import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="nav justify-content-end">
            <NavigationItem to="/" exact>Dishes</NavigationItem>
            <NavigationItem to="orders" exact>Orders</NavigationItem>
        </ul>
    );
};

export default NavigationItems;