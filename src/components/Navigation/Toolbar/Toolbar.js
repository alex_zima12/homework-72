import React from 'react';
import NavigationItems from "../NavigationItems/NavigationItems";


const Toolbar = props => {
    return (
        <>
            <nav className="navbar navbar-dark bg-primary ">
                <p className="font-weight-bold text-light container display-4">Turtle Pizza Admin</p>
                <NavigationItems />
            </nav>
        </>
    );
};

export default Toolbar;