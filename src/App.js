import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import DishBuilder from "./components/DishBuilder/DishBuilder";
import Orders from "./components/Orders/Orders";
import EditData from "./container/EditData/EditData";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={DishBuilder}/>
        <Route path="/orders" exact component={Orders}/>
          <Route path="/:id/edit" exact component={EditData}/>
        <Route render={() => <h1>404 Not Found</h1>}/>
      </Switch>
    </Layout>
);

export default App;
