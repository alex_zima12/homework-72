import {
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    FETCH_DISHES_FAILURE,
    SET_PURCHASING,
    REMOVE_DISH,
    EDIT_DISH
} from "../actions/actionTypes";

const initialState = {
    loading: false,
    error: null,
    fetchError: null,
    dishes: [],
    purchasing: false
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state,
                loading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state,
                loading: false,
                dishes: action.menu};
        case FETCH_DISHES_FAILURE:
            return {...state,
                loading: false,
                fetchError: action.error};
        case SET_PURCHASING:
            return {
                ...state,
                purchasing: action.purchasing
            };
        case REMOVE_DISH:
            let copyStateDishes = {...state.dishes};
            delete copyStateDishes[action.index]
            return {
                ...state,
                dishes: {...copyStateDishes}
            }
        case EDIT_DISH:
            let copyDishes = {...state.dishes};
            Object.keys(copyDishes).forEach(function (dish) {
                    if (action.id === dish) {
                        return {
                            ...state,
                            contacts: action.dish
                        }
                    }
                }
            ); return state
        default:
            return state;
    }
};

export default dishesReducer;