import {
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS,
    FETCH_ORDERS_FAILURE,
    REMOVE_ORDER

} from "../actions/actionTypes";

const initialState = {

    error: null,
    fetchError: null,
    orders: [],

};

const ordersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDERS_REQUEST:
            return {...state,
                loading: true};
        case FETCH_ORDERS_SUCCESS:

            return {...state,
                loading: false,
                orders: action.order};
        case FETCH_ORDERS_FAILURE:
            return {...state,
                loading: false,
                fetchError: action.error};
        case REMOVE_ORDER:
            let copyStateOrders = {...state.orders};
            delete copyStateOrders[action.id]
            return {
                ...state,
                orders: {...copyStateOrders}
            }
        default:
            return state;
    }
};

export default ordersReducer;