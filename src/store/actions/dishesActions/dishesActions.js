import {
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    SET_PURCHASING,
    REMOVE_DISH,
    EDIT_DISH

} from "../actionTypes";

import axiosPizza from "../../../axiosPizza";

const fetchDishesRequest = () => {
    return {type: FETCH_DISHES_REQUEST};
};
const fetchDishesSuccess = menu => {
    return {type: FETCH_DISHES_SUCCESS, menu};
};
const fetchDishesFailure = error => {
    return {type: FETCH_DISHES_FAILURE, error};
};

export const fetchDishes = () => {
    return async dispatch => {
        dispatch(fetchDishesRequest());
        try {
            const response = await axiosPizza.get('/turtlePizza.json');
            dispatch(fetchDishesSuccess(response.data));
        } catch(e) {
            dispatch(fetchDishesFailure(e));
        }

    };
};

export const setPurchasing = purchasing => {
    return {type: SET_PURCHASING, purchasing: purchasing}
};


export const removeDish= (index) => {
    return {type: REMOVE_DISH, index};

};


export const editDish = (id, dish) => {
    return {type: EDIT_DISH, id, dish};
};


