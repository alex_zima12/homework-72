export const FETCH_DISHES_REQUEST = "FETCH_DISHES_REQUEST";
export const FETCH_DISHES_SUCCESS = "FETCH_DISHES_SUCCESS";
export const FETCH_DISHES_FAILURE = "FETCH_DISHES_FAILURE";

export const SET_PURCHASING = "SET_PURCHASING";

export const REMOVE_DISH = "REMOVE_DISH";

export const EDIT_DISH = "EDIT_DISH";
export const REMOVE_ORDER = "REMOVE_ORDER";

export const FETCH_ORDERS_REQUEST = "FETCH_ORDERS_REQUEST";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_FAILURE = "FETCH_ORDERS_FAILURE";