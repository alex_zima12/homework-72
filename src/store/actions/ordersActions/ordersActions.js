import {
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS,
    FETCH_ORDERS_FAILURE,
    REMOVE_ORDER

} from "../actionTypes";
import axiosPizza from "../../../axiosPizza";

export  const fetchOrderRequest = () => {
    return {type: FETCH_ORDERS_REQUEST};
};
export const fetchOrderSuccess = order => {
    return {type: FETCH_ORDERS_SUCCESS, order};
};
export const fetchOrderFailure = error => {
    return {type: FETCH_ORDERS_FAILURE, error};
};

export const removeOrder = id => {
    return {type: REMOVE_ORDER, id};
};

export const fetchData = () => {
    return async dispatch => {
        dispatch(fetchOrderRequest());
        try {
            const response = await axiosPizza.get('/turtlePizzaOrders.json');
            dispatch(fetchOrderSuccess(response.data));
        } catch(e) {
            dispatch(fetchOrderFailure(e));
        }

    };
};