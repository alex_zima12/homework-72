import React, {useState} from 'react';
import {useSelector} from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import axiosPizza from "../../axiosPizza";

const DishData = (props) => {
    const [dishBuilder, setDishBuilder] = useState({
        title: '',
        price: '',
        image: ''
    });

    const loading = useSelector(state => state.dishes.loading);

    const dishBuilderChanged = event => {
        const name = event.target.name; // email
        const value = event.target.value;
        setDishBuilder(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const dishHandler = async event => {
        event.preventDefault();
        await axiosPizza.post('/turtlePizza.json', dishBuilder);
    };

let form = (
    <form>
        <div className="form-group">
            <label>Title</label>
            <input
                className="form-control" placeholder="Enter dish name"
                type="text" name="title"
                value={dishBuilder.title}
                onChange={dishBuilderChanged}
                required
            />
        </div>
        <div className="form-group">
            <label>Price</label>
            <input
                className="form-control" placeholder="Enter dish price"
                type="text" name="price"
                value={dishBuilder.price}
                onChange={dishBuilderChanged}
                required
            />
        </div>
        <div className="form-group">
            <label>Image</label>
            <input
                className="form-control" placeholder="https://example.com"
                type="url" name="image"
                value={dishBuilder.image}
                onChange={dishBuilderChanged}
                required
            />
        </div>
        <button
            className="btn btn-primary"
            onClick={dishHandler}
        >SAVE
        </button>
        <button
            className="btn btn-danger ml-3"
            onClick={props.purchaseCancelled}
        >CANCEL
        </button>
    </form>
);

if (loading) {
    form = <Spinner/>;
}

return (
    <>
        <h4>Enter data for dish</h4>
        {form}
    </>
);
}
;
export default DishData;
