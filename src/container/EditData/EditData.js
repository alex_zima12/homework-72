import React, {useEffect, useState} from 'react';
import axiosPizza from "../../axiosPizza";
import {useDispatch, useSelector} from "react-redux";
import {editDish} from "../../store/actions/dishesActions/dishesActions";

const EditData = props => {

    const dishes = useSelector( state => state.dishes.dishes);
    const dispatch = useDispatch();

    const [newTitle, setNewTitle] = useState('');
    const [newPrice, setNewPrice] = useState('');
    const [newImage, setNewImage] = useState('');

    useEffect(() => {
        Object.keys(dishes).forEach(function (dish) {
            if (props.match.params.id  === dish) {
                setNewTitle(dishes[dish].title);
                setNewPrice(dishes[dish].price);
                setNewImage(dishes[dish].image);

            }
        })
    },[dispatch,dishes,props.match.params.id ]);

    const changeTitle = event => {
        setNewTitle(event.target.value);
    };

    const changePrice = event => {
        setNewPrice(event.target.value);
    };

    const changeImage = event => {
        setNewImage(event.target.value);
    };
       const dish = {
        title: newTitle,
        price: newPrice,
        image: newImage
    };

    const changeDish = async (e, id)  => {
        e.preventDefault()
        await axiosPizza.put('/turtlePizza/' + id + '.json', dish);
        dispatch(editDish(id,dish));
        props.history.push('/');
    };

    const goBackToDishes = async event => {
        event.preventDefault();
        props.history.replace('/');
    };

    let form = (
        <form>
            <div className="form-group">
                <label>Title</label>
                <input
                    className="form-control" placeholder="Enter dish name"
                    type="text" name="title"
                    value={newTitle}
                    onChange={changeTitle}
                    required
                />
            </div>
            <div className="form-group">
                <label>Price</label>
                <input
                    className="form-control" placeholder="Enter dish price"
                    type="text" name="price"
                    value={newPrice}
                    onChange={changePrice}
                    required
                />
            </div>
            <div className="form-group">
                <label>Image</label>
                <input
                    className="form-control" placeholder="https://example.com"
                    type="url" name="image"
                    value={newImage}
                    onChange={changeImage}
                    required
                />
            </div>
            <button
                className="btn btn-primary"
                onClick={changeDish}
            >SAVE
            </button>
            <button
                className="btn btn-danger ml-3"
                onClick={goBackToDishes}
            >CANCEL
            </button>
        </form>
    );
    return (
        <div className="container mt-3">
            {form}
        </div>
    );
};

export default EditData;